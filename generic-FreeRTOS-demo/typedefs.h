#ifndef __typedefs_h
#define __typedefs_h

typedef     unsigned char       uint8_t;
typedef     unsigned short int  uint16_t;
typedef     unsigned int        uint32_t;
typedef     unsigned long int   uint64_t;

typedef     signed char         sint8_t;
typedef     signed short int    sint16_t;
typedef     signed int          sint32_t;
typedef     signed long int     sint64_t;


#endif //__typedefs_h