#include "isr.h"


void irq_uart0(void) __attribute__((naked));

void  irq_uart0  (void)  
{

    portSAVE_CONTEXT();		
    //if(IOPIN0&P23)IOCLR0 = P23; else IOSET0 = P23;
    volatile unsigned char u0iir=U0IIR,u0lsr=U0LSR,rxbyte,txbyte=0;
    portBASE_TYPE xHigherPriorityTaskWoken;
    if(u0iir) u0iir=u0iir; //prevent unused variable warning
    if(u0lsr) u0lsr=u0lsr; 
    
   if(u0iir&2)
    {
        while(U0LSR & 0x20){
    
            if(( xQueueReceiveFromISR( xQueueTX0,(unsigned char *) &( txbyte ),
                    ( portTickType ) 0 )) == pdTRUE){
                //if(IOPIN0&P23)IOCLR0 = P23; else IOSET0 = P23;
                U0THR=txbyte;
            } else{
                U0IER &= ~(0x2);
                break;
            }
        }
   }
     //rx fifo full | no data for a particular time
    if((u0iir&0x4)|(u0iir&0xC)){
    
        while (U0LSR & 0x01){
         
            rxbyte=U0RBR;
            xQueueSendToBackFromISR(xQueueRX0, (unsigned char *) &rxbyte,
                    &xHigherPriorityTaskWoken);
            
         		   		
        }
        
    }

    if( xHigherPriorityTaskWoken )
    {
        /* Actual macro used here is port specific. */
        //taskYIELD_FROM_ISR ();
    }
      
    VICVectAddr=0;     
    
	portRESTORE_CONTEXT();

}

void irq_eint (void) __attribute__((naked));

void irq_eint (void)  //__attribute__ ((interrupt("IRQ")))
{


	portSAVE_CONTEXT();	
	
    EXTINT=2;     //  tThisPoint - tButtonPress = 1.5us
                  // if pressed when in vTickISR = 5.5us (worst case scenario?)
                  
    
    if(IOPIN1&P16)IOCLR1 = P16; else IOSET1 = P16;
      
    VICVectAddr=0; 
    
    
	portRESTORE_CONTEXT();

}

/* 
 * The ISR used for the scheduler tick.
 */
void vTickISR( void ) __attribute__((naked));
void vTickISR( void )
{
    
	/* Save the context of the interrupted task. */
	portSAVE_CONTEXT();	

	/* Increment the RTOS tick count, then look for the highest priority 
	task that is ready to run. */
	__asm volatile( "bl vTaskIncrementTick" );

	#if configUSE_PREEMPTION == 1
		__asm volatile( "bl vTaskSwitchContext" );
	#endif
        //T0TC=0; //synch both timers
        
	T1_IR = portTIMER_MATCH_ISR_BIT;
	VICVectAddr = 0;
	
	/* Restore the context of the new task. */
	portRESTORE_CONTEXT();
	   
}
