#include "lpc.h"

void init_pio(void)
{

    PINSEL0 = 0;
    PINSEL1 = 0;
    PINSEL2 = 0;
    IODIR0  = 0;
    IODIR1  = 0;
    
    //           P0=>TXD0  |  P1=>RXD0  |  P2=SCL0   |  P3=>SDA0  | P4=>AD0.6  | P5=>MAT0.1 
    PINSEL0 |=  ( 1 << 0 ) | ( 1 << 2 ) | ( 1 << 4 ) | ( 1 << 6 ) | ( 3 << 8 ) | ( 2 << 10);
    
    //           P6=>AD1.0 |  P7=>PWM2  |  P8=TXD1   |  P9=>RXD1  | P10=>AD1.2 | P11=>CAP1.1
    PINSEL0 |=  ( 3 << 12) | ( 2 << 14) | ( 1 << 16) | ( 1 << 18) | ( 3 << 20) | ( 2 << 22);
    
    
    //         P12=>MAT1.0 | P13=>MAT1.1| P14=>EINT1 | P15=>AD1.5
    PINSEL0 |=  ( 2 << 24) | ( 2 << 26) | ( 2 << 28) | ( 3 << 30);  
    
    
    
    //         P16=>MAT0.2 |  P17=>SCK1 | P18=MISO1  | P19=>MOSI1 | P20=>SSEL1 | P21=>PWM5 
    PINSEL1 |=  ( 2 << 0 ) | ( 2 << 2 ) | ( 2 << 4 ) | ( 2 << 6 ) | ( 2 << 8 ) | ( 1 << 10);
    
    //         P22=>MAT0.0 | P23=>GPIO  |  P24=RESV  |  P25=>DAC  | P26=>AD0.5 | P27=>AD0.0
    PINSEL1 |=  ( 3 << 12) | ( 0 << 14) | ( 0 << 16) | ( 2 << 18) | ( 1 << 20) | ( 1 << 22);
    
    //          P28=>AD0.1 | P29=>MAT0.3| P30=>AD0.3 | P31=>GPO 
    PINSEL1 |=  ( 1 << 24) | ( 3 << 26) | ( 1 << 28) | ( 0 << 30);


    if (configSERVO_MUX_EN){
       
        //         P5=>MUX_C
        PINSEL0 &= ~(2 << 10);
        //         P29=>MUX_B
        PINSEL1 &= ~(3 << 26);    
        IODIR0 |= P29 | P5;
        IODIR1 |=    P24 ;  //output

    }

    if (configADC_DEMUX_EN){
    //         P30=>DEMUX_A     P28=>DEMUX_C    P26=>DEMUX_B
        PINSEL1 &= (~(1 << 28)) & (~(1 << 24)) & (~(1 << 20));
        IODIR0 |= P26 | P28 | P30;
    }

    //         RED_LED   GREEN_LED 
    IODIR0 |=   P23     |   P31   ;  //output
    
    //          EINT1
    IODIR0 &=   ~(P14)   ;      //input
    
    IODIR1 |=    P16    |   P17   |   P18  |   P19  |   P20  |   P21  |   P22  |   P23    ;  //output
    
}

void init_peripheral(void)
{
   
    init_timer();
    init_uart0();
    init_eint();
    
    //enableIRQ(); //***********
}

void init_uart0(void)
{

    extern void (irq_uart0 )( void );

    PINSEL0 |= 0x00000005;                     /* Enable RxD and TxD pins            */
    U0LCR = 0x83;                         /* 8 bits, no Parity, 1 Stop bit      */
    U0DLL = (configCPU_CLOCK_HZ/16/U0_BAUD_RATE) & 0xFF;  /* Setup Baudrate                 */
    U0DLM = ((configCPU_CLOCK_HZ/16/U0_BAUD_RATE) >> 8) & 0xFF;
    U0LCR = 0x03;                         /* DLAB = 0                           */
    
    
    U0IER = 0x2 | 0x1;       //THRIE TX int + RX int
    U0FCR |= 0xC7; //C7   //11000111  fifo enable  rx trigger level 14byte 
    U0TER |= 0x80; 
    
    
    VICIntSelect &= ~(1 << VIC_UART0);
    VICVectAddr6 = (unsigned int)irq_uart0;
    VICVectCntl6 = 0x20 | VIC_UART0; // Enable vector interrupt
    VICIntEnable |= (1<<VIC_UART0);  //enable
          
}

void init_timer(void)
{
        
    T0TCR   = 0;        // Disable timer
    T0PC    = 0; 
    T0PR    = 0x24E;//59;       //59/59MHz = 1us
    
    T1MCR  |= 0;//0x200;        // 001 000 000 000   interrupt disable
    T1CCR   = 0;        // Capture disable
    T1EMR   = 0x050;    // 0000 0101 0000  MAT1(0,1) => 1  clear on match  0x3
    
    T0MCR  |= 0;        // interrupt disable
    T0CCR   = 0;        // Capture disable
    if (configSERVO_MUX_EN){
        T0EMR   = 0x110;     // 0001 0001 0000  MAT0(0,2) => 1  clear on match  0x5        
    }else{
        T0EMR   = 0x550;    // 0101 0101 0000  MAT0(0..3) => 1  clear on match  0xF
    }
    
    T0TCR   = 2;        //reset   
    T0TCR   = 1;        //start    

}

void init_eint(void)
{

    extern void (irq_eint )( void );

    VICIntSelect &= ~(1 << VIC_EINT1);
    VICVectCntl7=0x20|VIC_EINT1;
    VICVectAddr7=( long ) irq_eint;
    VICIntEnable=1<<VIC_EINT1;
    
    EXTMODE     = 0x2; // 0010  EINT1 edge sensitive
    EXTPOLAR    = 0x0; // 0000  EINT1 interrupt on falling edge
    
}

//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~    VIC LOW LEVEL~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
#define IRQ_MASK 0x00000080
//unsigned enableIRQ(void);
//unsigned disableIRQ(void);
unsigned restoreIRQ(unsigned oldCPSR);

static inline unsigned asm_get_cpsr(void)
{
  unsigned long retval;
  asm volatile (" mrs  %0, cpsr" : "=r" (retval) : /* no inputs */  );
  return retval;
}

static inline void asm_set_cpsr(unsigned val)
{
  asm volatile (" msr  cpsr, %0" : /* no outputs */ : "r" (val)  );
}

unsigned enableIRQ(void)
{
  unsigned _cpsr;

  _cpsr = asm_get_cpsr();
  asm_set_cpsr(_cpsr & ~IRQ_MASK);
  return _cpsr;
}

unsigned disableIRQ(void)
{
  unsigned _cpsr;

  _cpsr = asm_get_cpsr();
  asm_set_cpsr(_cpsr | IRQ_MASK);
  return _cpsr;
}

unsigned restoreIRQ(unsigned oldCPSR)
{
  unsigned _cpsr;

  _cpsr = asm_get_cpsr();
  asm_set_cpsr((_cpsr & ~IRQ_MASK) | (oldCPSR & IRQ_MASK));
  return _cpsr;
}
/* end of R O code */

void init_low_level( void )
{
	#ifdef RUN_FROM_RAM
		/* Remap the interrupt vectors to RAM if we are are running from RAM. */
		SCB_MEMMAP = 2;
	#endif

	/* Setup the PLL to multiply the XTAL input by 4. */
	SCB_PLLCFG = ( mainPLL_MUL_4 | mainPLL_DIV_1 );

	/* Activate the PLL by turning it on then feeding the correct sequence of
	bytes. */
	SCB_PLLCON = mainPLL_ENABLE;
	SCB_PLLFEED = mainPLL_FEED_BYTE1;
	SCB_PLLFEED = mainPLL_FEED_BYTE2;

	/* Wait for the PLL to lock... */
	while( !( SCB_PLLSTAT & mainPLL_LOCK ) );

	/* ...before connecting it using the feed sequence again. */
	SCB_PLLCON = mainPLL_CONNECT;
	SCB_PLLFEED = mainPLL_FEED_BYTE1;
	SCB_PLLFEED = mainPLL_FEED_BYTE2;

	/* Setup and turn on the MAM.  Three cycle access is used due to the fast
	PLL used.  It is possible faster overall performance could be obtained by
	tuning the MAM and PLL settings. */
	MAM_TIM = mainMAM_TIM_3;
	MAM_CR = mainMAM_MODE_FULL;

	/* Setup the peripheral bus to be the same as the PLL output. */
	SCB_VPBDIV = mainBUS_CLK_FULL;
	
}
