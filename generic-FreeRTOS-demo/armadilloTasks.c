#include "armadilloTasks.h"


void init_armadillo_tasks(void)
{
    const unsigned char greeting[]="Armadillo Firmware v1.0\r";
    
    xTaskCreate(systask, ( sint8_t * ) "systask", configMINIMAL_STACK_SIZE,
            NULL, configMAX_USER_PRIORITY + 2 , NULL );

    xTaskCreate(task_serial_depack0, ( sint8_t * ) "depack",
            configMINIMAL_STACK_SIZE,NULL, configMAX_USER_PRIORITY + 1 , NULL );
    
    // Raw data to be put on U0THR
    xQueueTX0 = xQueueCreate( configUART0_BUFF_SIZE, sizeof( uint8_t ) );
    xQueueRX0 = xQueueCreate( configUART0_BUFF_SIZE, sizeof( uint8_t ) );
    
    xMutexTX0 = xSemaphoreCreateMutex();
    xMutexRX0 = xSemaphoreCreateMutex();
    
    U0THR='$';  // Bug
    
    if(xSemaphoreTake(xMutexTX0,( portTickType ) 1)){
        serial_pack((unsigned char *)greeting);
        xSemaphoreGive(xMutexTX0);
    }

    init_mux_demux();
    
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void init_mux_demux(void)
{
    unsigned char i;
    if (configSERVO_MUX_EN){
        rmask_mux_0 = MUX_B | MUX_C;
        rmask_mux_1 = MUX_A;
    }
    if (configADC_DEMUX_EN){
        rmask_demux = DEMUX_A | DEMUX_B | DEMUX_C;
    }
    // PWM0 => MAT0.0   PWM1 => MAT0.2   PWM2 => MAT1.0   PWM3 => MAT1.1
    for(i=0;i<8;i++){

        if((i%4)>1){ // 2 3 6 7
            mask_mux_0[i] = MUX_B;
            mask_demux[i] = DEMUX_B ;
        }else{
            mask_mux_0[i] = 0;
            mask_demux[i] = 0;
        }

        if(i>3){  // 4 5 6 7
            mask_mux_0[i] |= MUX_C;
            mask_demux[i] |= DEMUX_C ;
        }

        if(i%2){ // 1 3 5 7
            mask_mux_1[i]=MUX_A;
            mask_demux[i] |= DEMUX_A ;
        }else{
            mask_mux_1[i]=0;
        }

    }

    for(i=0;i<32;i++){
        servo[i]=140;
    }

}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
void systask( void *pvParameters )
{
    (void) pvParameters;
    portTickType xLastWakeTime;
    portTickType TickCount;
    const portTickType xFrequency = 1;
    

    xLastWakeTime = xTaskGetTickCount();

    for( ;; ){

        vTaskDelayUntil( &xLastWakeTime, xFrequency );
        TickCount = xTaskGetTickCount();
        unsigned char tick_cnt=TickCount%8,i;        

        if (configSERVO_MUX_EN){

            IOCLR0 = rmask_mux_0 - mask_mux_0[tick_cnt]; // clear mux sel bits
            IOCLR1 = rmask_mux_1 - mask_mux_1[tick_cnt];
            IOSET0 = mask_mux_0[tick_cnt];
            IOSET1 = mask_mux_1[tick_cnt];

            for(i=0;i<4;i++){
                mpwm[i]=servo[8*i+tick_cnt];
            }

            T0MR0=mpwm[0];
            T0MR2=mpwm[1];
            T1MR0=mpwm[2];
            T1MR1=mpwm[3];

        }else{

            T0MR0=servo[0];
            T0MR1=servo[1];
            T0MR2=servo[2];
            T0MR3=servo[3];
            T1MR0=servo[4];
            T1MR1=servo[5];
        }

        if (configADC_DEMUX_EN){
            IOCLR0 = rmask_demux - mask_demux[tick_cnt]; // clear mux sel bits
            IOSET0 = mask_demux[tick_cnt];
        }

        if (configSERVO_MUX_EN){
            T1EMR |= 0x3;
            T0EMR |= 0x5; //MAT0.1 => DEMUX_C MAT0.3 => DEMUX_B

        }else{
            if(tick_cnt == 7){
                T1EMR |= 0x3;
                T0EMR |= 0xF;
            }
	}

        T0TC=0; // Both timers are synced
        T1TC=0;
        /*
        while(U0LSR & 0x20){
            unsigned char txbyte=0;
            if(( xQueueReceive( xQueueUart0, &( txbyte ), ( portTickType ) 0 )) == pdTRUE){
                //if(IOPIN0&P23)IOCLR0 = P23; else IOSET0 = P23;
                U0THR=txbyte;
            } else{
                break;
            }
        }
        */




    }

}
void task_serial_depack0 ( void *pvParameters )
{
    (void) pvParameters;
    portTickType xLastWakeTime;
    
    const portTickType xFrequency = 1;
    
    xLastWakeTime = xTaskGetTickCount();

    for( ;; ){

        vTaskDelayUntil( &xLastWakeTime, xFrequency );
        uint8_t rxbyte=0;

        while( xQueueReceive( xQueueRX0, &( rxbyte ), ( portTickType ) 0 )){
            if(rxbyte=='$'){
                if(IOPIN0&P23)IOCLR0 = P23; else IOSET0 = P23;
            }

        }
        

    }

}
void serial_pack(unsigned char txbuff[])
{
    
    unsigned char available=0,i=0;
    while(txbuff[i]){
        available=1;
        xQueueSendToBack(xQueueTX0, (unsigned char *) &(txbuff[i++]),
                    ( portTickType ) 0);
    }

    if(available){        
        U0IER |= 0x2;
    }
}