#ifndef __main_h
#define __main_h


/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

//Project includes
#include "LPC2000.h"
#include "board.h"
#include "lpc.h"
#include "isr.h"
//#include "global.h"
#include "armadilloConfig.h"
#include "armadilloTasks.h"
#include "typedefs.h"


static void led_blink( void *pvParameters );
static void task2( void *pvParameters );



#endif  //__main_h

