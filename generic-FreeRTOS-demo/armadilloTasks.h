#ifndef __armadillo_tasks_h
#define __armadillo_tasks_h

/* Standard includes. */
#include <stdlib.h>
#include <string.h>
#include <stdio.h>

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"
#include "queue.h"
#include "semphr.h"

//Project includes
#include "LPC2000.h"
#include "board.h"
#include "lpc.h"
#include "isr.h"
//#include "global.h"
#include "armadilloConfig.h"
#include "typedefs.h"

extern void init_armadillo_tasks(void);

uint16_t servo[32];
uint16_t mpwm[4];
extern uint16_t adc[32];

void systask( void *pvParameters );
void init_mux_demux(void);
extern void serial_pack(uint8_t txbuff[]);
void task_serial_depack0(void *pvParameters);


unsigned int mask_mux_0[8],mask_mux_1[8],mask_demux[8]; //set masks
unsigned int rmask_mux_0,rmask_mux_1,rmask_demux; //reset masks

xSemaphoreHandle xMutexTX0,xMutexRX0;
xQueueHandle xQueueTX0,xQueueRX0;


#endif //__armadillo_tasks_h
