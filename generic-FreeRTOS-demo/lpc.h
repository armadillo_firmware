#ifndef __lpc_h
#define __lpc_h

/* Scheduler includes. */
#include "FreeRTOS.h"
#include "task.h"

//Project includes
#include "LPC2000.h"
#include "board.h"
#include "isr.h"
#include "FreeRTOSConfig.h"
#include "armadilloConfig.h"
#include "typedefs.h"

/* Constants to setup the PLL. */
#define mainPLL_MUL_4		( ( unsigned char ) 0x0003 )
#define mainPLL_DIV_1		( ( unsigned char ) 0x0000 )
#define mainPLL_ENABLE		( ( unsigned char ) 0x0001 )
#define mainPLL_CONNECT		( ( unsigned char ) 0x0003 )
#define mainPLL_FEED_BYTE1	( ( unsigned char ) 0xaa )
#define mainPLL_FEED_BYTE2	( ( unsigned char ) 0x55 )
#define mainPLL_LOCK		( ( unsigned long ) 0x0400 )

/* Constants to setup the MAM. */
#define mainMAM_TIM_3		( ( unsigned char ) 0x03 )
#define mainMAM_MODE_FULL	( ( unsigned char ) 0x02 )

/* Constants to setup the peripheral bus. */
#define mainBUS_CLK_FULL	( ( unsigned char ) 0x01 )

#define VIC_TIMER1       5  /* Timer 1 (Match 0-3 Capture 0-3)    */
#define VIC_UART0        6
#define VIC_EINT1       15
#define U0_BAUD_RATE 57600
#define U1_BAUD_RATE 57600

typedef void (*fp2)(void);

extern void init_low_level( void );
extern void init_pio( void );
extern void init_peripheral(void);


void init_timer(void);
void init_uart0(void);

void init_eint(void);

unsigned enableIRQ(void);
unsigned disableIRQ(void);

#endif  //__lpc_h
