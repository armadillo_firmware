#ifndef __armadillo_config_h
#define __armadillo_config_h

#define configSERVO_MUX_EN      1
#define configADC_DEMUX_EN      0
#define configUART0_BUFF_SIZE   32
#define configMAX_USER_PRIORITY 4


#endif //__armadillo_config_h
