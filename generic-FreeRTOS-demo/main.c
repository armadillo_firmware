#include "main.h"

unsigned int compareMatch =0;

int main( void )
{
    
    init_low_level();   // CPU Frequency & PLL
    init_pio();         // Map port pins to selected functions
    init_peripheral();  // UART,Timer,Extint,ADC Enable
    init_armadillo_tasks();    
    
    xTaskCreate(led_blink, ( signed char * ) "Blinky", configMINIMAL_STACK_SIZE
            ,NULL, tskIDLE_PRIORITY + 2 , NULL );
    xTaskCreate(task2, ( signed char * ) "task2", configMINIMAL_STACK_SIZE
            ,NULL, tskIDLE_PRIORITY + 3 , NULL );
    
    vTaskStartScheduler();
	
    return 0; // Should not reach here unless scheduler fails
	
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
static void led_blink( void *pvParameters )
{
    (void) pvParameters;  //Prevent warning

    portTickType xLastWakeTime;
    portTickType TickCount;
    const portTickType xFrequency = 1; //50
    unsigned int cnt=0;    
    
    xLastWakeTime = xTaskGetTickCount();

    for( ;; ){       

        vTaskDelayUntil( &xLastWakeTime, xFrequency );        
        TickCount = xTaskGetTickCount();
        
        char txbuff[configUART0_BUFF_SIZE];

        if(cnt%8==0){    // takes 110 us
            sprintf(txbuff,"$%4d %s\r",cnt/8,"seconds");

            if(xSemaphoreTake(xMutexTX0,( portTickType ) 1)){
                serial_pack((unsigned char *)txbuff);
                xSemaphoreGive(xMutexTX0);
            }
           
        }        

        if(cnt%8<4){ // Heart Beat
            if(IOPIN0&S_LED)IOCLR0 = S_LED; else IOSET0 = S_LED;
        }

        cnt++;        

    }

}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
static void task2( void *pvParameters )
{
    (void) pvParameters;  //Prevent warning

    portTickType xLastWakeTime;
    portTickType TickCount;
    const portTickType xFrequency = 9; //50
    unsigned int cnt=0;

    xLastWakeTime = xTaskGetTickCount();

    for( ;; ){

        vTaskDelayUntil( &xLastWakeTime, xFrequency );
        TickCount = xTaskGetTickCount();

        char txbuff[configUART0_BUFF_SIZE];
        
        sprintf(txbuff,"$%4d %s\r",cnt,"task2");

        if(xSemaphoreTake(xMutexTX0,( portTickType ) 1)){
            serial_pack((unsigned char *)txbuff);
            xSemaphoreGive(xMutexTX0);
        }
        cnt++;
    }

}
