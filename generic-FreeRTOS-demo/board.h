#ifndef __board_h
#define __board_h

#define P0	0x1
#define P1	0x2
#define P2	0x4
#define P3	0x8
#define P4	0x10
#define P5	0x20
#define P6	0x40
#define P7	0x80
#define P8	0x100
#define P9	0x200
#define P10	0x400
#define P11	0x800
#define P12	0x1000
#define P13	0x2000
#define P14	0x4000
#define P15	0x8000
#define P16	0x10000
#define P17	0x20000
#define P18	0x40000
#define P19	0x80000
#define P20	0x100000
#define P21	0x200000
#define P22	0x400000
#define P23	0x800000
#define P24	0x1000000
#define P25	0x2000000
#define P26	0x4000000
#define P27	0x8000000
#define P28	0x10000000
#define P29	0x20000000
#define P30	0x40000000
#define P31	0x80000000

#define S_LED   P31

#define MUX_A   P24 //P1.24
#define MUX_B   P29
#define MUX_C   P5
#define DEMUX_A     P28
#define DEMUX_B     P20
#define DEMUX_C     P24

#endif  //__board_h
