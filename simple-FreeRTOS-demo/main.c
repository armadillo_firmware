#include "main.h"

int main( void )
{
    
    init_low_level();	
	
    xTaskCreate(led_blink, ( signed char * ) "Blink", configMINIMAL_STACK_SIZE, NULL, tskIDLE_PRIORITY + 3 , NULL );        
	
	vTaskStartScheduler();
	
	return 0;
	
}
//~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
static void led_blink( void *pvParameters )
{

    IODIR0 |= P10;

    for( ;; ){   
            
        vTaskDelay( 50);
        if(IOPIN0&P10) IOCLR0 = P10;  else IOSET0 = P10;  	 
                  
    }

}





